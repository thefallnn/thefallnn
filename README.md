<!-- Profile -->
<p align="left"><strong><samp>「</samp></strong></p>
  <p align="center">
    <samp>
      <b>
        Hello There
      <br>
        I'm Sourav, a developer, a student, and a Linux enthusiast.
      </b>
      <br>
        <image src="https://readme-typing-svg.herokuapp.com?font=Fira+Code&weight=700&duration=2000&pause=500&color=1d2021&center=true&width=435&lines=The+King+Of+The+Fall">
      <br>
      <b>
        ~ Sourav (TheFallnn) Gope ~
      </b>
    </samp>
  </p>
<p align="right"><strong><samp>」</samp></strong></p>

<br>

<details align="center">
<summary></summary>

<h2></h2><br>

<!-- Contact Me -->
<p align="center">
  <samp>
    [<a href="https://twitter.com/thefallnn">twitter</a>]
    [<a href="https://matrix.to/#/@thefallnn:matrix.org">matrix</a>] <!-- we cannot change our username on matrix it seems -->
    [<a href="mailto:sourav@thefallnn.tech">e-mail</a>]
  </samp>
</p>

<h2></h2><br>

<!-- Github Stats -->
<p align="center">
  <samp>
    <img src="https://komarev.com/ghpvc/?username=thefallnn&label=Profile+Views&color=689d6a" alt="thefallnn" /> 
  </samp>
</p>
<p align="center">
  <samp>
    <details>
      <summary>My Profile Stats</summary>
        <br>
        <img alt="GitHub Stats" src="https://github-readme-stats.vercel.app/api?username=thefallnn&show_icons=true&include_all_commits=true&count_private=true&hide=issues&hide_border=true&theme=gruvbox_light"/>
    </details>
    <details> 
      <summary>My Most Used Languages</summary>
        <br>
        <img alt="Top Language" src="https://github-readme-stats.vercel.app/api/top-langs/?username=thefallnn&layout=compact&hide_border=true&theme=gruvbox_light"/>
        <br>
        <b>Note:</b> Top languages is only a metric of the languages my public code consists of and doesn't reflect experience or skill level.
    </details>
  </samp>
</p>

<h2></h2><br>

<!-- GPG Keys -->

```sh
curl -sS https://github.com/thefallnn.gpg | gpg --import
```
```console
C2C4 AB08 8A62 28C1 F7E3  6541 5716 9934 0987 4F80
```

</details>

